package com.example.lucia.segvideo.functionalities.algorithm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.projectMenu.ProjectMenu;

/**
 * Created by lucia on 10/10/17.
 */

public class AlgorithmActivity extends AppCompatActivity {

    String projectName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_choose_algorithm);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            projectName = bundle.getString("name");
        }

        Log.w("Project Name", projectName);

    }

    public void algorithmOriginal(View view){
        Intent intent = new Intent(this, ProjectMenu.class);
        intent.putExtra("name", projectName);
        intent.putExtra("algorithm", "original");
        Toast.makeText(this, "Algorithm Original",Toast.LENGTH_SHORT).show();
        this.startActivity(intent);
    }

    public void algorithmDNLM(View view){
        Intent intent = new Intent(this, ProjectMenu.class);
        intent.putExtra("name", projectName);
        intent.putExtra("algorithm", "DNLM");
        Toast.makeText(this, "Algorithm DNLM",Toast.LENGTH_SHORT).show();
        this.startActivity(intent);
    }

    public void algorithmDBF(View view){
        Intent intent = new Intent(this, ProjectMenu.class);
        intent.putExtra("name", projectName);
        intent.putExtra("algorithm", "DBF");
        Toast.makeText(this, "Algorithm DBF",Toast.LENGTH_SHORT).show();
        this.startActivity(intent);

    }




}
