package com.example.lucia.segvideo.functionalities.project;

import android.view.View;
/**
 * Created by lucia on 10/6/17.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean inLongClick);
}

