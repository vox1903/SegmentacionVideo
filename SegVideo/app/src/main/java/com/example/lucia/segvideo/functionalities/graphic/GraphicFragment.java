package com.example.lucia.segvideo.functionalities.graphic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.MainActivity;
import com.example.lucia.segvideo.functionalities.project.ProjectActivity;

import org.json.JSONObject;

/**
 * Created by lucia on 10/1/17.
 */

public class GraphicFragment extends Fragment {

    private String projectName;
    private String algorithm;
    private View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle b = getActivity().getIntent().getExtras();

        if(b != null){
            Log.w("bundle", "yes");
            projectName = b.getString("name");
            algorithm = b.getString("algorithm");
        }

        Log.w("grafic", projectName);
        Log.w("grafic", algorithm);

        rootView =  inflater.inflate(R.layout.fragment_graphics, container, false);

        AQuery aq = new AQuery(this.getActivity());
        String url = "http://10.0.3.2:3000/diagram?name=" + projectName + "&algorithm=" + algorithm;
        aq.ajax(url, JSONObject.class, this, "jsonCallback");

        return rootView;
    }


    public void jsonCallback(String url, JSONObject json, AjaxStatus status) {
        if (json != null) {
            Log.w("entre", "no null");
            try {
                String image = json.getString("android_image");
                Log.w("Imagen", image);
                ImageView myImage = (ImageView)rootView.findViewById(R.id.image_graph);
                int drawableResourceId = getResources().getIdentifier(image, "drawable", getActivity().getPackageName());
                myImage.setImageResource(drawableResourceId);

            } catch (Exception ex) {
                Log.w("entre", "catch");
            }
        } else{
            Log.w("entre", "null :(");
        }
    }


}
