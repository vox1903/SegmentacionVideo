package com.example.lucia.segvideo.functionalities.views;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.data.project.Project;


import java.util.ArrayList;
import java.util.List;
/**
 * Created by lucia on 10/6/17.
 */

public class ProjectRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context context;
    List<String> mListProject = new ArrayList<>();

    public ProjectRecyclerViewAdapter(Context context, List<String> listProject){
        this.context = context;
        this.mListProject = listProject;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.list_item_project, parent, false);
        Item item = new Item(row);
        return item ;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Item)holder).textView.setText(mListProject.get(position));
    }

    @Override
    public int getItemCount() {
        return mListProject.size();
    }

    public class Item extends RecyclerView.ViewHolder{
        TextView textView;
        public Item(View itemView){
            super (itemView);
            textView = (TextView) itemView.findViewById(R.id.textViewNameProject);

        }
    }
}
