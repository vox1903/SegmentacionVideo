package com.example.lucia.segvideo.functionalities.information;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.lucia.segvideo.R;

import org.json.JSONObject;

/**
 * Created by lucia on 10/1/17.
 */

public class InformationFragment extends Fragment {

    String projectName;
    String algorithm;
    TextView generatedVideos;
    TextView averageScenes;
    TextView deviationScenes;
    TextView averageCuts;
    TextView deviationCuts;
    TextView averageNoCuts;
    TextView deviationNoCuts;
    TextView executionTime;
    TextView sigma1;
    TextView sigma2;
    TextView delta;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view1 = inflater.inflate(R.layout.fragment_information, container, false);
        init(view1);
        return view1;
    }

    public void init(View view1){
        generatedVideos = (TextView)view1.findViewById(R.id.generated_videos);
        averageScenes = (TextView)view1.findViewById(R.id.average_scenes);
        deviationScenes = (TextView)view1.findViewById(R.id.deviation_scenes);
        averageCuts = (TextView)view1.findViewById(R.id.average_cuts);
        deviationCuts= (TextView)view1.findViewById(R.id.deviation_cuts);
        averageNoCuts = (TextView)view1.findViewById(R.id.average_noCuts);
        deviationNoCuts = (TextView)view1.findViewById(R.id.deviation_noCuts);
        executionTime = (TextView)view1.findViewById(R.id.execution_time);
        sigma1 = (TextView)view1.findViewById(R.id.sigma1);
        sigma2 = (TextView)view1.findViewById(R.id.sigma2);
        delta = (TextView)view1.findViewById(R.id.delta);
        Bundle b = getActivity().getIntent().getExtras();

        if(b != null){
            projectName = b.getString("name");
            algorithm = b.getString("algorithm");
        }

        AQuery aq = new AQuery(this.getActivity());
        String url = "http://10.0.3.2:3000/project/" + projectName + "/" + algorithm;
        aq.ajax(url, JSONObject.class, this, "jsonCallback");
    }

    public void jsonCallback(String url, JSONObject json, AjaxStatus status) {
        if (json != null) {
            Log.w("entre", "no null");
            try {
                //Log.w("algorithm", json.toString());
                generatedVideos.setText(json.get("totalSegments").toString());
                averageScenes.setText(json.get("average_scenes").toString());
                deviationScenes.setText(json.get("deviation_scenes").toString());
                averageCuts.setText(json.get("average_cuts").toString());
                deviationCuts.setText(json.get("deviation_cuts").toString());
                averageNoCuts.setText(json.get("average_noCuts").toString());
                deviationNoCuts.setText(json.get("deviation_noCuts").toString());
                executionTime.setText(json.get("execution_time").toString());
                sigma1.setText(json.get("sigma1").toString());
                sigma2.setText(json.get("sigma2").toString());
                delta.setText(json.get("delta").toString());

            } catch (Exception ex) {
                Log.w("entre", "catch");
            }
        } else{
            Log.w("entre", "null :(");
        }
    }


}
