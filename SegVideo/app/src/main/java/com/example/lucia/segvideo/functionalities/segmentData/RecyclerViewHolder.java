package com.example.lucia.segvideo.functionalities.segmentData;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.project.ItemClickListener;

/**
 * Created by lucia on 10/13/17.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

    public TextView txt_name;
    public TextView txt_position;
    public TextView txt_cut;
    public TextView txt_duration;


    public RecyclerViewHolder(View itemView) {
        super(itemView);
        txt_name = (TextView)itemView.findViewById(R.id.textViewSegment);
        txt_position = (TextView)itemView.findViewById(R.id.textViewNumberSegment);
        txt_cut = (TextView)itemView.findViewById(R.id.textViewFrameCut);
        txt_duration = (TextView)itemView.findViewById(R.id.textViewDuration);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        Log.w("aaKSKKSKALD","ddddddd");

    }

    public void setItemClickListener(ItemClickListener itemClickListener){
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }
}