package com.example.lucia.segvideo.functionalities.segmentData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.lucia.segvideo.functionalities.data.segment.Segment;
import com.example.lucia.segvideo.functionalities.projectMenu.ProjectMenu;
import com.example.lucia.segvideo.R;

/**
 * Created by lucia on 10/1/17.
 */

public class SegmentFragment extends Fragment {

    private RecyclerView mRecyclerView;

    List<Segment> mListSegment = new ArrayList<Segment>();

    private SegmentActivity mActivity =  new SegmentActivity();

    private String projectName;
    private String algorithm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup  container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_segments, container, false);

        ProjectMenu data = (ProjectMenu) getActivity();
        projectName = data.getProject();
        algorithm = data.getAlgorithm();

        Log.w("Segment",projectName);
        Log.w("Segment",algorithm);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_segment);
//        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        RecyclerAdapter adapter = new RecyclerAdapter(view.getContext(),mListSegment);
        mRecyclerView.setAdapter(adapter);

//        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_segment);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()) );
//        RecyclerAdapter adapter = new RecyclerAdapter(mListSegment);
//        recyclerView.setAdapter(adapter);
//        layotManager = new LinearLayoutManager(this.getContext());
//        recyclerView.setLayoutManager(layotManager);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setAdapter(adapter);

        loadquery();


        return view;
    }

    public void loadquery(){

        AQuery aq = new AQuery(this.getActivity());
        String url = "http://10.0.3.2:3000/segments?nameProject=" + projectName + "&algorithm=" + algorithm + "";
        aq.ajax(url, JSONObject.class, this, "jsonCallback");
    }

    public void jsonCallback(String url, JSONObject json, AjaxStatus status) {
        if (json != null) {
            try {
                String segments = json.getString("segments");
                Log.w("SEGMENT", segments);
                creatListSegments(json);
//                Toast.makeText(this, "ok", Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                Log.w("SEGMENT", "catch");
            }
        } else{
            Log.w("SEGMENT", "No SEGMENT");
        }
    }

    public void creatListSegments(JSONObject json) throws JSONException {
        JSONArray json_array = json.optJSONArray("segments"); //cada uno de los elementos dentro de la etiqueta proyecto

        for (int i = 0; i < json_array.length(); i++) {
            mListSegment.add(new Segment(i,json_array.getJSONObject(i))); //Insertamos los datos

        }

    }


    public static void viewProject(String s) {
        Log.w("NOMBRE DE SEGMENTO: ", s);
    }





}
