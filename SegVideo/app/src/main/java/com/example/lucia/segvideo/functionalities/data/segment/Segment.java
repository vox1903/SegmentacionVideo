package com.example.lucia.segvideo.functionalities.data.segment;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;

/**
 * Created by lucia on 10/12/17.
 */

public class Segment extends RealmObject {


    private int id;
    private String name;
    private String frame_cut;
    private String duration;



    public Segment(){

    }

    public Segment(int id, JSONObject json) throws JSONException {
        this.id = id;
        this.name = json.getString("name");
        this.frame_cut = json.getString("frame_cut");
        this.duration = json.getString("duration");
    }

    public Segment(String name, String frame_cut, String duration) {
        this.name = name;
        this.frame_cut = frame_cut;
        this.duration = duration;
    }
    public Segment(int id, String name, String frame_cut, String duration) {
        this.id = id;
        this.name = name;
        this.frame_cut = frame_cut;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrame_cut() {
        return frame_cut;
    }

    public void setFrame_cut(String frame_cut) {
        this.frame_cut = frame_cut;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
