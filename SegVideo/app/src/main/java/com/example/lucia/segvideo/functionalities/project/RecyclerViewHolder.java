package com.example.lucia.segvideo.functionalities.project;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.example.lucia.segvideo.R;
import android.widget.TextView;

/**
 * Created by lucia on 10/6/17.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public TextView txt_name;
    public TextView txt_position;


    private ItemClickListener itemClickListener;
    public RecyclerViewHolder(View itemView) {
        super(itemView);
        txt_name = (TextView)itemView.findViewById(R.id.textViewNameProject);
        txt_position = (TextView)itemView.findViewById(R.id.textViewNumber);


        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

    @Override
    public boolean onLongClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), true);
        return true ;
    }
}
