package com.example.lucia.segvideo.functionalities.project;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.data.project.Project;
import com.example.lucia.segvideo.functionalities.graphic.GraphicActivity;
import com.example.lucia.segvideo.functionalities.graphic.GraphicFragment;
import com.example.lucia.segvideo.functionalities.information.InformationFragment;
//import com.example.lucia.segvideo.functionalities.project.ProjectFragment;
import com.example.lucia.segvideo.functionalities.projectMenu.ProjectMenu;
import com.example.lucia.segvideo.functionalities.views.ProjectRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by lucia on 9/29/17.
 */

public class ProjectActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;

    List<Project> mListProject = new ArrayList<Project>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_projects);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_project);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this) );
        RecyclerAdapter adapter = new RecyclerAdapter(this,mListProject);
        mRecyclerView.setAdapter(adapter);

        loadquery();
    }

    public void loadquery(){

        AQuery aq = new AQuery(this);
        String url = "http://10.0.3.2:3000/project";
        aq.ajax(url, JSONObject.class, this, "jsonCallback");
    }

    public void jsonCallback(String url, JSONObject json, AjaxStatus status) {
        if (json != null) {
            try {
                String projcts = json.getString("projects");
                Log.w("PROJECT", projcts);
                creatListProjects(json);
//                Toast.makeText(this, "ok", Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                Log.w("PROJECT", "catch");
            }
        } else{
            Log.w("PROJECT", "No Project");
        }
    }

    public void creatListProjects(JSONObject json) throws JSONException {
        JSONArray json_array = json.optJSONArray("projects"); //cada uno de los elementos dentro de la etiqueta proyecto

        for (int i = 0; i < json_array.length(); i++) {
            mListProject.add(new Project(i,json_array.getJSONObject(i))); //Insertamos los datos

        }
    }


    public static void viewProject(String s) {
        Log.w("NOMBRE DE PROYECTO: ", s);
    }
}
