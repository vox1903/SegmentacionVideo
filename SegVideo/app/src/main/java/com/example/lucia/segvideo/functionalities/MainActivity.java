 package com.example.lucia.segvideo.functionalities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.project.ProjectActivity;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;


 public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        final Button mButtonLogin = (Button) findViewById(R.id.buttonLogin);


        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buclick(v);
            }
        });
    }


    public void buclick(View view){
        EditText userEmail = (EditText) findViewById(R.id.editTextEmail);
        EditText userPassword = (EditText) findViewById(R.id.editTextPassword);

        AQuery aq = new AQuery(this);
        String url = "http://10.0.3.2:3000/users?email=" + userEmail.getText() + "&password=" + userPassword.getText();
//        String url = "http://10.0.3.2:3000/users?name=lucia&password=lucia";
        aq.ajax(url, JSONObject.class, this, "jsonCallback");
    }

     public void jsonCallback(String url, JSONObject json, AjaxStatus status) {
         if (json != null) {
             Log.w("entre", "no null");
             try {
                 String msg = json.getString("msg");
                 String resul = String.valueOf(Toast.makeText(this, msg, Toast.LENGTH_LONG));
                 if(msg.equals("Thanks, login info is correct")){
                    Intent intent = new Intent(MainActivity.this, ProjectActivity.class);
                    startActivity(intent) ;
                 }
                 Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

             } catch (Exception ex) {
                 Log.w("entre", "catch");
             }
         } else{
             Log.w("entre", "null :(");
         }
     }

}
