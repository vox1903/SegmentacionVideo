package com.example.lucia.segvideo.functionalities.data.project;

/**
 * Created by lucia on 10/5/17.
 */
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcel;

import java.util.Objects;

//import io.realm.ProjectRealmProxy;
import io.realm.RealmObject;


public class Project extends RealmObject{


    private int id;
    private String name;


    public Project(){

    }

    public Project(int id, JSONObject json) throws JSONException {
        this.id = id;
        this.name = json.getString("name");
    }

    public Project(String name) {
        this.name = name;
    }
    public Project(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
