package com.example.lucia.segvideo.functionalities.segmentData;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;



import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.algorithm.AlgorithmActivity;
import com.example.lucia.segvideo.functionalities.data.segment.Segment;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucia on 10/6/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private List<Segment> mListSegment = new ArrayList<>();
    private Context context;


    public RecyclerAdapter(Context context, List<Segment> listSegment){
        this.context = context;
        this.mListSegment = listSegment ;
        Log.w("aaKSKKSKALD","cccccccc ");

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.list_item_segment, parent, false);
        Log.w("aaKSKKSKALD","bbbbbbbbb");
        return new RecyclerViewHolder(row) ;


    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        final String name = mListSegment.get(position).getName();
        int posi = mListSegment.get(position).getId();
        final String cut = mListSegment.get(position).getFrame_cut();
        final String duration = mListSegment.get(position).getDuration();

        String pos = Integer.toString(posi);
        Log.w("aaKSKKSKALD","AAAAAAAAAA");
        Log.w("aaKSKKSKALD", name);

        holder.txt_name.setText(name);
        holder.txt_position.setText(pos + ":");
        holder.txt_cut.setText(cut);
        holder.txt_duration.setText(duration);



    }

    @Override
    public int getItemCount() {
        return mListSegment.size();
    }


}