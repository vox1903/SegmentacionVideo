-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: segVideo
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `algorithm`
--

DROP TABLE IF EXISTS `algorithm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `algorithm` (
  `algorithm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`algorithm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `algorithm`
--

LOCK TABLES `algorithm` WRITE;
/*!40000 ALTER TABLE `algorithm` DISABLE KEYS */;
INSERT INTO `algorithm` VALUES (1,'original'),(2,'dnlm'),(3,'dbf');
/*!40000 ALTER TABLE `algorithm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `algorithmState`
--

DROP TABLE IF EXISTS `algorithmState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `algorithmState` (
  `algorithmState_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`algorithmState_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `algorithmState`
--

LOCK TABLES `algorithmState` WRITE;
/*!40000 ALTER TABLE `algorithmState` DISABLE KEYS */;
INSERT INTO `algorithmState` VALUES (1,'Processing');
/*!40000 ALTER TABLE `algorithmState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `algorithm_project`
--

DROP TABLE IF EXISTS `algorithm_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `algorithm_project` (
  `algorithm_project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `algorithm_id` int(11) unsigned NOT NULL,
  `project_id` int(11) unsigned NOT NULL,
  `metric_id` int(11) unsigned NOT NULL,
  `improvement` double DEFAULT NULL,
  `algorithmState_id` int(11) unsigned DEFAULT NULL,
  `sigma1` double DEFAULT '0',
  `sigma2` double DEFAULT '0',
  `delta` double DEFAULT '0',
  `average_cuts` double DEFAULT NULL,
  `deviation_cuts` double DEFAULT NULL,
  `average_noCuts` double DEFAULT NULL,
  `deviation_noCuts` double DEFAULT NULL,
  `path_diagram` varchar(200) DEFAULT NULL,
  `average_scenes` double DEFAULT NULL,
  `execution_time` double DEFAULT NULL,
  `deviation_scenes` double DEFAULT NULL,
  `diagram_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`algorithm_project_id`),
  KEY `algorithm_id` (`algorithm_id`),
  KEY `metrics_id` (`metric_id`),
  KEY `project_id` (`project_id`),
  KEY `algorithmState_id` (`algorithmState_id`),
  CONSTRAINT `algorithm_project_ibfk_1` FOREIGN KEY (`algorithm_id`) REFERENCES `algorithm` (`algorithm_id`),
  CONSTRAINT `algorithm_project_ibfk_4` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`metric_id`),
  CONSTRAINT `algorithm_project_ibfk_5` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`),
  CONSTRAINT `algorithm_project_ibfk_6` FOREIGN KEY (`algorithmState_id`) REFERENCES `algorithmState` (`algorithmState_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `algorithm_project`
--

LOCK TABLES `algorithm_project` WRITE;
/*!40000 ALTER TABLE `algorithm_project` DISABLE KEYS */;
INSERT INTO `algorithm_project` VALUES (49,1,67,49,NULL,1,0,0,0,0.5,0.11,0.04,0.03,'/projects/Test1/Original/segDistances.png',8,41.21,4.8,NULL),(50,3,67,50,NULL,1,10,14,14.5,0,0,0.03,0,'/projects/Test1/DBF/segDistances.png',132,33.41,0,NULL),(51,2,67,51,NULL,1,6,13,8.5,0,0,0,0,'/projects/Test1/DNLM/segDistances.png',132,113.11,0,NULL);
/*!40000 ALTER TABLE `algorithm_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `configuration_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `excelFormat` varchar(100) NOT NULL DEFAULT '',
  `latexFormat` varchar(100) NOT NULL DEFAULT '',
  `groundtruthFormat` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metric`
--

DROP TABLE IF EXISTS `metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recall` double DEFAULT NULL,
  `precision` double DEFAULT NULL,
  `false_positives` double DEFAULT NULL,
  `false_negatives` double DEFAULT NULL,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metric`
--

LOCK TABLES `metric` WRITE;
/*!40000 ALTER TABLE `metric` DISABLE KEYS */;
INSERT INTO `metric` VALUES (49,0,0,0,0),(50,0,0,0,0),(51,0,0,0,0);
/*!40000 ALTER TABLE `metric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `path` varchar(200) NOT NULL DEFAULT '',
  `imageFileName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (67,'Test1','public/projects/Test1','/projects/Test1/projectImage.png');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segment`
--

DROP TABLE IF EXISTS `segment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segment` (
  `segment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(200) NOT NULL,
  `frame_cut` int(11) NOT NULL,
  `algorithm_project_id` int(11) unsigned DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`segment_id`),
  KEY `algorithm_project_id` (`algorithm_project_id`),
  CONSTRAINT `segment_ibfk_1` FOREIGN KEY (`algorithm_project_id`) REFERENCES `algorithm_project` (`algorithm_project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segment`
--

LOCK TABLES `segment` WRITE;
/*!40000 ALTER TABLE `segment` DISABLE KEYS */;
INSERT INTO `segment` VALUES (176,'seg0.mp4','/projects/Test1/Original/seg0.mp4',453,49,17,'/projects/Test1/Original/seg0.png'),(177,'seg1.mp4','/projects/Test1/Original/seg1.mp4',710,49,10,'/projects/Test1/Original/seg1.png'),(178,'seg2.mp4','/projects/Test1/Original/seg2.mp4',836,49,5,'/projects/Test1/Original/seg2.png'),(179,'seg3.mp4','/projects/Test1/Original/seg3.mp4',1069,49,9,'/projects/Test1/Original/seg3.png'),(180,'seg4.mp4','/projects/Test1/Original/seg4.mp4',1277,49,8,'/projects/Test1/Original/seg4.png'),(181,'seg5.mp4','/projects/Test1/Original/seg5.mp4',1504,49,9,'/projects/Test1/Original/seg5.png'),(182,'seg6.mp4','/projects/Test1/Original/seg6.mp4',1769,49,10,'/projects/Test1/Original/seg6.png'),(183,'seg7.mp4','/projects/Test1/Original/seg7.mp4',1865,49,4,'/projects/Test1/Original/seg7.png'),(184,'seg8.mp4','/projects/Test1/Original/seg8.mp4',1870,49,1,'/projects/Test1/Original/seg8.png'),(185,'seg9.mp4','/projects/Test1/Original/seg9.mp4',2001,49,5,'/projects/Test1/Original/seg9.png'),(186,'seg10.mp4','/projects/Test1/Original/seg10.mp4',2006,49,1,'/projects/Test1/Original/seg10.png'),(187,'seg11.mp4','/projects/Test1/Original/seg11.mp4',2215,49,8,'/projects/Test1/Original/seg11.png'),(188,'seg12.mp4','/projects/Test1/Original/seg12.mp4',2576,49,13,'/projects/Test1/Original/seg12.png'),(189,'seg13.mp4','/projects/Test1/Original/seg13.mp4',2856,49,10,'/projects/Test1/Original/seg13.png'),(190,'seg14.mp4','/projects/Test1/Original/seg14.mp4',3336,49,18,'/projects/Test1/Original/seg14.png'),(191,'seg15.mp4','/projects/Test1/Original/seg15.mp4',3683,49,13,'/projects/Test1/Original/seg15.png'),(192,'seg0.mp4','/projects/Test1/DBF/seg0.mp4',3683,50,132,'/projects/Test1/DBF/seg0.png'),(193,'seg0.mp4','/projects/Test1/DNLM/seg0.mp4',3683,51,132,'/projects/Test1/DNLM/seg0.png');
/*!40000 ALTER TABLE `segment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentType`
--

DROP TABLE IF EXISTS `segmentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentType` (
  `segmentType_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`segmentType_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentType`
--

LOCK TABLES `segmentType` WRITE;
/*!40000 ALTER TABLE `segmentType` DISABLE KEYS */;
/*!40000 ALTER TABLE `segmentType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(253) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'paul','123');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `video_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `imageFileName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (1,'test.mp4',NULL),(2,'test2.mp4',NULL);
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'segVideo'
--
/*!50003 DROP PROCEDURE IF EXISTS `checkUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkUser`(
	IN `pEmail` VARCHAR(45),
	IN `pPassword` VARCHAR(45))
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT u.email
		FROM user u
		WHERE u.email = pEmail AND u.password = pPassword;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkVideo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkVideo`(
	IN `pName` VARCHAR(45))
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT COUNT(v.name) as 'count'
		FROM video v
		WHERE v.name = pName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getProjectData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProjectData`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
SELECT 
	ap.`sigma1`,
	ap.`sigma2`,
	ap.`delta`,
	ap.`average_scenes`,
	ap.`deviation_scenes`,
	ap.`average_cuts`,
	ap.`deviation_cuts`,
	ap.`average_noCuts`,
	ap.`deviation_noCuts`,
	ap.`execution_time`,
	ap.`improvement`,
	COUNT(*) as totalSegments
	FROM (SELECT project_id FROM project WHERE name = pNameProject) p
	INNER JOIN algorithm_project ap
	ON p.project_id = ap.project_id
	INNER JOIN
	(SELECT algorithm_id FROM algorithm WHERE name = pAlgorithm) a
	ON a.algorithm_id = ap.algorithm_id
	INNER JOIN segment s
	ON s.`algorithm_project_id` = ap.`algorithm_project_id`
	GROUP BY ap.`algorithm_project_id`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getProjectResult` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProjectResult`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET DATA FROM SELECTED PROJECT ALGORITHM
-- ------------------------------------------------------------
		SELECT p.name,ps.name as `algorithmState`,
		ap.`average_scenes`,
		ap.`deviation_scenes`,
		ap.`average_cuts`,
		ap.`deviation_cuts`,
		ap.`average_noCuts`,
		ap.`deviation_noCuts`,
		ap.`execution_time`,
		ap.`improvement`,
		ap.`sigma1`,
		ap.`sigma2`,
        ap.`delta`
		FROM 
		(select algorithm_id from algorithm where name = pAlgorithm) as a,
		(select name,`project_id` from project p where name = pNameProject) as p,
		algorithm_project ap,algorithmState ps
		WHERE ap.algorithm_id = a.algorithm_id AND p.project_id = ap.project_id AND ps.algorithmState_id = ap.algorithmState_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getProjects` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProjects`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT p.name , p.imageFileName
		FROM project p;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getSegments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSegments`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET SEGMENTS OF SPECIFIC PROJECT
-- ------------------------------------------------------------
SELECT s.name,s.frame_cut,s.duration,s.path,s.image
	FROM (SELECT project_id FROM project WHERE name = pNameProject) p
	INNER JOIN algorithm_project ap
	ON p.project_id = ap.project_id
	INNER JOIN
	(SELECT algorithm_id FROM algorithm WHERE name = pAlgorithm) a
	ON a.algorithm_id = ap.algorithm_id
	INNER JOIN segment s
	WHERE s.`algorithm_project_id` = ap.`algorithm_project_id`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getVideos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getVideos`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT v.`name`
		FROM video v;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertNewProject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewProject`(
	IN `pName` VARCHAR(45),
	IN `pPath` VARCHAR(200),
	IN `pImageFileName` VARCHAR(200),
	IN `pSigma1Dbf` DOUBLE,
	IN `pSigma2Dbf` DOUBLE,
	IN `pDeltaDbf` DOUBLE,
	IN `pSigma1Dnlm` DOUBLE,
	IN `pSigma2Dnlm` DOUBLE,
	IN `pDeltaDnlm` DOUBLE
)
BEGIN
-- ------------------------------------------------------------
-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
-- ------------------------------------------------------------
		DECLARE `_rollback` BOOL DEFAULT 0;
	    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
		START TRANSACTION;    

        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsOriginal = LAST_INSERT_ID();
        
         INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDBF = LAST_INSERT_ID();
        
        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDNLM = LAST_INSERT_ID();
        	
		INSERT INTO project (`name`,`path`,`imageFileName`)
        	VALUES(pName,pPath,pImageFileName);
        SET @projectId = LAST_INSERT_ID();
        
        	-- ORIGINAL
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'Original'),
	        @projectId,
	        @metricsOriginal,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	        
	        -- DBF
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,`metric_id`,
     	`sigma1`,
        `sigma2`,
		`delta`,
	     `algorithmState_id`)  
	        VALUES((select `algorithm_id` from algorithm where name = 'DBF'),
	        @projectId,
	        @metricsDBF,
	        pSigma1Dbf,
	        pSigma2Dbf,
	        pDeltaDbf,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	                
	        -- DNLM
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`sigma1`,
        `sigma2`,
		`delta`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'DNLM'),
	        @projectId,
	        @metricsDNLM,
	        pSigma1Dnlm,
	        pSigma2Dnlm,
	        pDeltaDnlm,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
		IF `_rollback` THEN
	        ROLLBACK;
    	ELSE
        	COMMIT;
	    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertNewSegment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewSegment`(
		IN `pNameProject` VARCHAR(45),
		IN `pAlgorithm`    VARCHAR(45),
		IN `pNameSegment`  VARCHAR(200),
		IN `pPathSegment`  VARCHAR(200),
		IN `pFrameCut` INT,
		IN `pDuration` INT,
        IN `pImage` VARCHAR(200)
	)
BEGIN
	-- ------------------------------------------------------------
	-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
	-- ------------------------------------------------------------
			DECLARE `_rollback` BOOL DEFAULT 0;
		    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
	        
			INSERT INTO segment (`name`,`path`,`frame_cut`,`duration`,`image`,`algorithm_project_id`)
	        	VALUES(
	        		pNameSegment,
	        		pPathSegment,
	        		pFrameCut,
	        		pDuration,
                    pImage,
	        		(SELECT algorithm_project_id
	        		FROM algorithm_project ap,
	        		(select algorithm_id from algorithm where name = pAlgorithm) as a,
					(select name,`project_id` from project p where name = pNameproject) as p
	        	 	WHERE ap.`project_id` = p.`project_id` AND ap.`algorithm_id` = a.`algorithm_id`)
		       	 );
	        
			IF `_rollback` THEN
		        ROLLBACK;
	    	ELSE
	        	COMMIT;
		    END IF;
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchProjects` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchProjects`(
	IN `pName` VARCHAR(45))
BEGIN

		SELECT p.name , p.imageFileName
		FROM project p
		WHERE p.name LIKE CONCAT(pName,"%");
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setDataProject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `setDataProject`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45),
	IN `pAverageScenes` DOUBLE,
	IN `pDeviationScenes` DOUBLE,
	IN `pAverageCuts` DOUBLE,
	IN `pDeviationCuts` DOUBLE,
	IN `pAverageNoCuts` DOUBLE,
	IN `pDeviationNoCuts` DOUBLE,
	IN `pExecutionTime` DOUBLE,
	IN `pPathDiagram` VARCHAR(200)
)
BEGIN
-- ------------------------------------------------------------
-- Description: SET PROJECT DATA
-- ------------------------------------------------------------

	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;

	UPDATE algorithm_project ap,
			(select algorithm_id from algorithm where name = pAlgorithm) as a,
			(select name,`project_id` from project p where name = pNameProject) as p
	SET
		`average_scenes` 	= pAverageScenes,
		`deviation_scenes` 	= pDeviationScenes,
		`average_cuts` 		= pAverageCuts,
		`deviation_cuts` 	= pDeviationCuts,
		`average_noCuts` 	= pAverageNoCuts,
		`deviation_noCuts` 	= pDeviationNoCuts,
		`execution_time` 	= pExecutionTime,
		`path_diagram`		= pPathDiagram
	WHERE
		ap.`project_id` = p.`project_id` AND ap.`algorithm_id` = a.`algorithm_id`;
		
	IF `_rollback` THEN
		ROLLBACK;
	ELSE
	    COMMIT;
	END IF;
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateProjectImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProjectImage`(
	IN pProject VARCHAR(45),
    IN pImage VARCHAR(200)
)
BEGIN
	UPDATE project
    SET imageFileName = pImage
    WHERE name = pProject;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-04  0:33:01
