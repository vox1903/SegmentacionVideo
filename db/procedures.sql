DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewProject`(
	IN `pName` VARCHAR(45),
	IN `pPath` VARCHAR(200),
	IN `pImageFileName` VARCHAR(200)
)
BEGIN
-- ------------------------------------------------------------
-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
-- ------------------------------------------------------------
		DECLARE `_rollback` BOOL DEFAULT 0;
	    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
		START TRANSACTION;    

        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsOriginal = LAST_INSERT_ID();
        
         INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDBF = LAST_INSERT_ID();
        
        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDNLM = LAST_INSERT_ID();
        	
		INSERT INTO project (`name`,`path`,`imageFileName`,`projectState_id`)
        	VALUES(pName,pPath,pImageFileName,(select `projectState_id` from projectState where name = 'Processing'));
        SET @projectId = LAST_INSERT_ID();
        
        	-- ORIGINAL
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'Original'),
	        @projectId,
	        @metricsOriginal,
	        0.0,
	        0.0,
	        0.0);
	        
	        -- DBF
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'DBF'),
	        @projectId,
	        @metricsDBF,
	        0.0,
	        0.0,
	        0.0);	
	                
	        -- DNLM
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'DNLM'),
	        @projectId,
	        @metricsDNLM,
	        0.0,
	        0.0,
	        0.0);
		IF `_rollback` THEN
	        ROLLBACK;
    	ELSE
        	COMMIT;
	    END IF;
END ;;
DELIMITER ;

call insertNewProject('Books','projects/test/','soccer.jpg');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProjects`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT p.name , p.imageFileName
		FROM project p;
END ;;
DELIMITER ;

call getProjects();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getVideos`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT v.`name`
		FROM video v;
END ;;
DELIMITER ;

call getVideos();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getVideoPath`(
	IN `pName` VARCHAR(45))
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT v.`path`
		FROM video v
		WHERE v.name = pName;
END ;;
DELIMITER ;

call getVideoPath('test.mp4');
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

call getSegments('soccer1','original')


DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSegments`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET SEGMENTS OF SPECIFIC PROJECT
-- ------------------------------------------------------------
		SELECT s.name,s.frameCut,s.duration,s.path
		FROM project p
		INNER JOIN algorithm_project ap
		ON p.project_id = ap.project_id AND p.`name` = pNameProject
		INNER JOIN algorithm a 
		ON a.`name` = pAlgorithm AND ap.`algorithm_id` = a.`algorithm_id`
		INNER JOIN segment s
		ON s.algorithm_project_id = ap.algorithm_project_id;
END;;
DELIMITER ;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////


DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNewProject`(
	IN `pName` VARCHAR(45),
	IN `pPath` VARCHAR(200),
	IN `pImageFileName` VARCHAR(200),
	IN `pSigma1Dbf` DOUBLE,
	IN `pSigma2Dbf` DOUBLE,
	IN `pDeltaDbf` DOUBLE,
	IN `pSigma1Dnlm` DOUBLE,
	IN `pSigma2Dnlm` DOUBLE,
	IN `pDeltaDnlm` DOUBLE
)
BEGIN
-- ------------------------------------------------------------
-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
-- ------------------------------------------------------------
		DECLARE `_rollback` BOOL DEFAULT 0;
	    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
		START TRANSACTION;    

        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsOriginal = LAST_INSERT_ID();
        
         INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDBF = LAST_INSERT_ID();
        
        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDNLM = LAST_INSERT_ID();
        	
		INSERT INTO project (`name`,`path`,`imageFileName`)
        	VALUES(pName,pPath,pImageFileName);
        SET @projectId = LAST_INSERT_ID();
        
        	-- ORIGINAL
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'Original'),
	        @projectId,
	        @metricsOriginal,
	        0.0,
	        0.0,
	        0.0,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	        
	        -- DBF
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`,
     	`sigma1`,
        `sigma2`,
		`delta`,
	     `algorithmState_id`)  
	        VALUES((select `algorithm_id` from algorithm where name = 'DBF'),
	        @projectId,
	        @metricsDBF,
	        0.0,
	        0.0,
	        0.0,
	        pSigma1Dbf,
	        pSigma2Dbf,
	        pDeltaDbf,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	                
	        -- DNLM
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`execution_time`,
     	`improvement`,
     	`average_energy`,
     	`sigma1`,
        `sigma2`,
		`delta`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'DNLM'),
	        @projectId,
	        @metricsDNLM,
	        0.0,
	        0.0,
	        0.0,
	        pSigma1Dnlm,
	        pSigma2Dnlm,
	        pDeltaDnlm,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
		IF `_rollback` THEN
	        ROLLBACK;
    	ELSE
        	COMMIT;
	    END IF;
END;;
DELIMITER ;



call getSegments('Prueba1','dbf');


SELECT s.name,s.frame_cut,s.duration,s.path
		FROM project p
		INNER JOIN algorithm_project ap
		ON p.project_id = ap.project_id AND p.`name` = pNameProject
		INNER JOIN algorithm a 
		ON a.`name` = pAlgorithm AND ap.`algorithm_id` = a.`algorithm_id`
		INNER JOIN segment s
		ON s.algorithm_project_id = ap.algorithm_project_id;
		
		
SELECT s.name,s.frame_cut,s.duration,s.path
	FROM (SELECT project_id FROM project WHERE name = pNameProject) p
	INNER JOIN algorithm_project ap
	ON p.project_id = ap.project_id
	INNER JOIN
	(SELECT algorithm_id FROM algorithm WHERE name = pAlgorithm) a
	ON a.algorithm_id = ap.algorithm_id
	INNER JOIN segment s
	WHERE s.`algorithm_project_id` = ap.`algorithm_project_id`;
	
	
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSegments`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET SEGMENTS OF SPECIFIC PROJECT
-- ------------------------------------------------------------
SELECT s.name,s.frame_cut,s.duration,s.path
	FROM (SELECT project_id FROM project WHERE name = pNameProject) p
	INNER JOIN algorithm_project ap
	ON p.project_id = ap.project_id
	INNER JOIN
	(SELECT algorithm_id FROM algorithm WHERE name = pAlgorithm) a
	ON a.algorithm_id = ap.algorithm_id
	INNER JOIN segment s
	WHERE s.`algorithm_project_id` = ap.`algorithm_project_id`;
END;;
DELIMITER ;

