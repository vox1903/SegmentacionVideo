# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: segVideo
# Generation Time: 2017-10-06 19:44:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table algorithm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `algorithm`;

CREATE TABLE `algorithm` (
  `algorithm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`algorithm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `algorithm` WRITE;
/*!40000 ALTER TABLE `algorithm` DISABLE KEYS */;

INSERT INTO `algorithm` (`algorithm_id`, `name`)
VALUES
	(1,'original'),
	(2,'dnlm'),
	(3,'dbf');

/*!40000 ALTER TABLE `algorithm` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table algorithm_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `algorithm_project`;

CREATE TABLE `algorithm_project` (
  `algorithm_project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `algorithm_id` int(11) unsigned NOT NULL,
  `project_id` int(11) unsigned NOT NULL,
  `metric_id` int(11) unsigned NOT NULL,
  `improvement` double DEFAULT NULL,
  `algorithmState_id` int(11) unsigned DEFAULT NULL,
  `sigma1` double DEFAULT NULL,
  `sigma2` double DEFAULT NULL,
  `delta` double DEFAULT NULL,
  `average_cuts` double DEFAULT NULL,
  `deviation_cuts` double DEFAULT NULL,
  `average_noCuts` double DEFAULT NULL,
  `deviation_noCuts` double DEFAULT NULL,
  `path_diagram` varchar(200) DEFAULT NULL,
  `average_scenes` double DEFAULT NULL,
  `execution_time` double DEFAULT NULL,
  `deviation_scenes` double DEFAULT NULL,
  PRIMARY KEY (`algorithm_project_id`),
  KEY `algorithm_id` (`algorithm_id`),
  KEY `metrics_id` (`metric_id`),
  KEY `project_id` (`project_id`),
  KEY `algorithmState_id` (`algorithmState_id`),
  CONSTRAINT `algorithm_project_ibfk_1` FOREIGN KEY (`algorithm_id`) REFERENCES `algorithm` (`algorithm_id`),
  CONSTRAINT `algorithm_project_ibfk_4` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`metric_id`),
  CONSTRAINT `algorithm_project_ibfk_5` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`),
  CONSTRAINT `algorithm_project_ibfk_6` FOREIGN KEY (`algorithmState_id`) REFERENCES `algorithmState` (`algorithmState_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `algorithm_project` WRITE;
/*!40000 ALTER TABLE `algorithm_project` DISABLE KEYS */;

INSERT INTO `algorithm_project` (`algorithm_project_id`, `algorithm_id`, `project_id`, `metric_id`, `improvement`, `algorithmState_id`, `sigma1`, `sigma2`, `delta`, `average_cuts`, `deviation_cuts`, `average_noCuts`, `deviation_noCuts`, `path_diagram`, `average_scenes`, `execution_time`, `deviation_scenes`)
VALUES
	(22,1,58,22,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,3,58,23,NULL,1,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,2,58,24,NULL,1,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `algorithm_project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table algorithmState
# ------------------------------------------------------------

DROP TABLE IF EXISTS `algorithmState`;

CREATE TABLE `algorithmState` (
  `algorithmState_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`algorithmState_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `algorithmState` WRITE;
/*!40000 ALTER TABLE `algorithmState` DISABLE KEYS */;

INSERT INTO `algorithmState` (`algorithmState_id`, `name`)
VALUES
	(1,'Processing');

/*!40000 ALTER TABLE `algorithmState` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table configuration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `configuration`;

CREATE TABLE `configuration` (
  `configuration_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `excelFormat` varchar(100) NOT NULL DEFAULT '',
  `latexFormat` varchar(100) NOT NULL DEFAULT '',
  `groundtruthFormat` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table metric
# ------------------------------------------------------------

DROP TABLE IF EXISTS `metric`;

CREATE TABLE `metric` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recall` double DEFAULT NULL,
  `precision` double DEFAULT NULL,
  `false_positives` double DEFAULT NULL,
  `false_negatives` double DEFAULT NULL,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `metric` WRITE;
/*!40000 ALTER TABLE `metric` DISABLE KEYS */;

INSERT INTO `metric` (`metric_id`, `recall`, `precision`, `false_positives`, `false_negatives`)
VALUES
	(19,0,0,0,0),
	(20,0,0,0,0),
	(21,0,0,0,0),
	(22,0,0,0,0),
	(23,0,0,0,0),
	(24,0,0,0,0);

/*!40000 ALTER TABLE `metric` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `path` varchar(200) NOT NULL DEFAULT '',
  `imageFileName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`project_id`, `name`, `path`, `imageFileName`)
VALUES
	(58,'Prueba1','public/projects/Prueba1','soccer.jpg');

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table segment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `segment`;

CREATE TABLE `segment` (
  `segment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `path` varchar(200) NOT NULL,
  `frame_cut` int(11) NOT NULL,
  `algorithm_project_id` int(11) unsigned DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`segment_id`),
  KEY `algorithm_project_id` (`algorithm_project_id`),
  CONSTRAINT `segment_ibfk_1` FOREIGN KEY (`algorithm_project_id`) REFERENCES `algorithm_project` (`algorithm_project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `segment` WRITE;
/*!40000 ALTER TABLE `segment` DISABLE KEYS */;

INSERT INTO `segment` (`segment_id`, `name`, `path`, `frame_cut`, `algorithm_project_id`, `duration`)
VALUES
	(38,'original','',0,22,NULL),
	(39,'original','',0,22,NULL),
	(40,'original','',0,22,NULL),
	(41,'dbf','',0,23,NULL),
	(42,'dbf','',0,23,NULL),
	(43,'dnlm','',0,24,NULL),
	(44,'dnlm','',0,24,NULL);

/*!40000 ALTER TABLE `segment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table segmentType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `segmentType`;

CREATE TABLE `segmentType` (
  `segmentType_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`segmentType_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(253) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table video
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video`;

CREATE TABLE `video` (
  `video_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `imageFileName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;

INSERT INTO `video` (`video_id`, `name`, `imageFileName`)
VALUES
	(1,'test.mp4',NULL),
	(2,'test2.mp4',NULL);

/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping routines (PROCEDURE) for database 'segVideo'
--
DELIMITER ;;

# Dump of PROCEDURE checkVideo
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `checkVideo` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `checkVideo`(
	IN `pName` VARCHAR(45))
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT COUNT(v.name) as 'count'
		FROM video v
		WHERE v.name = pName;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE getProjectResult
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `getProjectResult` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `getProjectResult`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET DATA FROM SELECTED PROJECT ALGORITHM
-- ------------------------------------------------------------
		SELECT p.name,ps.name as `algorithmState`,
		ap.`average_scenes`,
		ap.`deviation_scenes`,
		ap.`average_cuts`,
		ap.`deviation_cuts`,
		ap.`average_noCuts`,
		ap.`deviation_noCuts`,
		ap.`execution_time`,
		ap.`improvement`,
		ap.`sigma1`,
		ap.`sigma2`
		FROM 
		(select algorithm_id from algorithm where name = pAlgorithm) as a,
		(select name,`project_id` from project p where name = pNameProject) as p,
		algorithm_project ap,algorithmState ps
		WHERE ap.algorithm_id = a.algorithm_id AND p.project_id = ap.project_id AND ps.algorithmState_id = ap.algorithmState_id;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE getProjects
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `getProjects` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `getProjects`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT p.name , p.imageFileName
		FROM project p;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE getSegments
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `getSegments` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `getSegments`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45)
)
BEGIN
-- ------------------------------------------------------------
-- Description: GET SEGMENTS OF SPECIFIC PROJECT
-- ------------------------------------------------------------
SELECT s.name,s.frame_cut,s.duration,s.path
	FROM (SELECT project_id FROM project WHERE name = pNameProject) p
	INNER JOIN algorithm_project ap
	ON p.project_id = ap.project_id
	INNER JOIN
	(SELECT algorithm_id FROM algorithm WHERE name = pAlgorithm) a
	ON a.algorithm_id = ap.algorithm_id
	INNER JOIN segment s
	WHERE s.`algorithm_project_id` = ap.`algorithm_project_id`;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE getVideos
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `getVideos` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `getVideos`()
BEGIN
-- ------------------------------------------------------------
-- Description: SELECTS PROJECTS AND IMAGES
-- ------------------------------------------------------------
		SELECT v.`name`
		FROM video v;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE insertNewProject
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `insertNewProject` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `insertNewProject`(
	IN `pName` VARCHAR(45),
	IN `pPath` VARCHAR(200),
	IN `pImageFileName` VARCHAR(200),
	IN `pSigma1Dbf` DOUBLE,
	IN `pSigma2Dbf` DOUBLE,
	IN `pDeltaDbf` DOUBLE,
	IN `pSigma1Dnlm` DOUBLE,
	IN `pSigma2Dnlm` DOUBLE,
	IN `pDeltaDnlm` DOUBLE
)
BEGIN
-- ------------------------------------------------------------
-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
-- ------------------------------------------------------------
		DECLARE `_rollback` BOOL DEFAULT 0;
	    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
		START TRANSACTION;    

        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsOriginal = LAST_INSERT_ID();
        
         INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDBF = LAST_INSERT_ID();
        
        INSERT INTO metric (`precision`,`recall`,`false_negatives`,`false_positives`)
        	VALUES(0.0,0.0,0.0,0.0);
        SET @metricsDNLM = LAST_INSERT_ID();
        	
		INSERT INTO project (`name`,`path`,`imageFileName`)
        	VALUES(pName,pPath,pImageFileName);
        SET @projectId = LAST_INSERT_ID();
        
        	-- ORIGINAL
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'Original'),
	        @projectId,
	        @metricsOriginal,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	        
	        -- DBF
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,`metric_id`,
     	`sigma1`,
        `sigma2`,
		`delta`,
	     `algorithmState_id`)  
	        VALUES((select `algorithm_id` from algorithm where name = 'DBF'),
	        @projectId,
	        @metricsDBF,
	        pSigma1Dbf,
	        pSigma2Dbf,
	        pDeltaDbf,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
	                
	        -- DNLM
     	INSERT INTO algorithm_project (`algorithm_id`,
     	`project_id`,
     	`metric_id`,
     	`sigma1`,
        `sigma2`,
		`delta`,
     	`algorithmState_id`) 
	        VALUES((select `algorithm_id` from algorithm where name = 'DNLM'),
	        @projectId,
	        @metricsDNLM,
	        pSigma1Dnlm,
	        pSigma2Dnlm,
	        pDeltaDnlm,
	        (select `algorithmState_id` from algorithmState where name = 'Processing'));
		IF `_rollback` THEN
	        ROLLBACK;
    	ELSE
        	COMMIT;
	    END IF;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE insertNewSegment
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `insertNewSegment` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `insertNewSegment`(
		IN `pNameProject` VARCHAR(45),
		IN `pAlgorithm`    VARCHAR(45),
		IN `pNameSegment`  VARCHAR(200),
		IN `pPathSegment`  VARCHAR(200),
		IN `pFrameCut` INT,
		IN `pDuration` INT
	)
BEGIN
	-- ------------------------------------------------------------
	-- Description: INSERTS NEW PROJECT WITH ALGORITHMS AND METRICS
	-- ------------------------------------------------------------
			DECLARE `_rollback` BOOL DEFAULT 0;
		    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
	        
			INSERT INTO segment (`name`,`path`,`frame_cut`,`duration`,`algorithm_project_id`)
	        	VALUES(
	        		pNameSegment,
	        		pPathSegment,
	        		pFrameCut,
	        		pDuration,
	        		(SELECT algorithm_project_id
	        		FROM algorithm_project ap,
	        		(select algorithm_id from algorithm where name = pAlgorithm) as a,
					(select name,`project_id` from project p where name = pNameproject) as p
	        	 	WHERE ap.`project_id` = p.`project_id` AND ap.`algorithm_id` = a.`algorithm_id`)
		       	 );
	        
			IF `_rollback` THEN
		        ROLLBACK;
	    	ELSE
	        	COMMIT;
		    END IF;
	END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE setDataProject
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `setDataProject` */;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `setDataProject`(
	IN `pNameProject` VARCHAR(45),
	IN `pAlgorithm` VARCHAR(45),
	IN `pAverageScenes` DOUBLE,
	IN `pDeviationScenes` DOUBLE,
	IN `pAverageCuts` DOUBLE,
	IN `pDeviationCuts` DOUBLE,
	IN `pAverageNoCuts` DOUBLE,
	IN `pDeviationNoCuts` DOUBLE,
	IN `pExecutionTime` DOUBLE,
	IN `pPathDiagram` VARCHAR(200)
)
BEGIN
-- ------------------------------------------------------------
-- Description: SET PROJECT DATA
-- ------------------------------------------------------------

	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;

	UPDATE algorithm_project ap,
			(select algorithm_id from algorithm where name = pAlgorithm) as a,
			(select name,`project_id` from project p where name = pNameProject) as p
	SET
		`average_scenes` 	= pAverageScenes,
		`deviation_scenes` 	= pDeviationScenes,
		`average_cuts` 		= pAverageCuts,
		`deviation_cuts` 	= pDeviationCuts,
		`average_noCuts` 	= pAverageNoCuts,
		`deviation_noCuts` 	= pDeviationNoCuts,
		`execution_time` 	= pExecutionTime,
		`path_diagram`		= pPathDiagram
	WHERE
		ap.`project_id` = p.`project_id` AND ap.`algorithm_id` = a.`algorithm_id`;
		
	IF `_rollback` THEN
		ROLLBACK;
	ELSE
	    COMMIT;
	END IF;
		
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
