# Install python-opencv

## Install python
+ Dowload [python 2.7.x](https://www.python.org/downloads/)
+ Install  Python at C:\Python

## Install numpy
+ Open cmd and go to folder C:\Python27\Scripts
+ Type ```pip install numpy```
+ Check if numpy works, open python prompt and run:

```python
import numpy
print numpy.__version__
```

It should print out the numpy version you have installed.


## Install openCV
Download [openCV](http://opencv.org/releases.html)


Extract opencv to C drive, then copy and paste the cv2.pyd   file from `C:\opencv\build\python\2.7\(x86 or x64) depending of your system` to directory ```C:\Python27\Lib\site-packages```.

  
## Set Enviromental Variables
On windows search, type `enviroment` and click the option "Edit the system environment variables", then click on  "Environment Variables". Then select Path on system variables and click on edit, then add the following directories:

+ C:\Python27\Scripts\
+ C:\Python27\
+ C:\opencv\build\bin\
+ C:\opencv\sources\3rdparty\ffmpeg\
+ C:\opencv\build\x64\vc14\

Now that you have to add `%OPENCV_DIR%\bin` to the User Variable PATH. 

Test if openCV its working, open python prompt and run:

```python
import cv2
print cv2.__version__
```

It should print out the openCV version you downloaded.

## Test if OpenCV and the FFMPEG codec are working
+ Create a new file and call it `test.py`.
+ Place the video you want to read at the same directory.
+ Run the code:

```python
import cv2
import numpy as np
 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture("your_video.mp4")
 
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
 
    # Display the resulting frame
    cv2.imshow('Frame',frame)
 
    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()
```

>Code downloaded from https://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/

