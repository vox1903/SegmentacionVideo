var express = require('express');
var router = express.Router();

var groundTruthController = require("./../controllers/groundTruthController.js");

//--STATES--
const state = {
	projectName : "",
	algorithm: "",
	cuts : [],
	metrics : []
}

function checkCuts(req,res,next){
	if (!state.cuts.length)
		res.render('message',{message : "Cuts no loaded"})
	next();
}

router.get('/:projectName/compare', checkCuts, function(req, res){
	console.log(state);
	res.render('message',{message : "Cuts loaded"});
});

router.get('/:projectName/:algorithm', function(req, res){
	state.projectName = req.params.projectName;
	state.algorithm = req.params.algorithm;
	res.render('groundTruth',state);
});

router.post('/:projectName/:algorithm', function(req, res){
	state.cuts = req.body.fragments;
	groundTruthController.generateGroundTruth(state,function(){
		groundTruthController.updateMetrics(state, function(){
    		res.render('metrics',state);
  		});
	});
});

module.exports = router;






