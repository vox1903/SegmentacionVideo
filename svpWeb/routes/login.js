var express = require('express');
var router = express.Router();

var loginController = require("./../controllers/loginController.js");

//--STATES--
const state = {
  logged : false,
	tries : 0,
  email : "",
  password : ""
} 

router.get('/logout',function(req, res){
  if (req.session.user_email){
    req.session.user_email = 0;
    res.redirect("back");
  }
});


router.get('/', function(req, res){
  if (req.session.user_email) {
    res.redirect("configuration");
  } else {
		res.render("login");
  }
});

router.post('/',function(req, res){
  state.email = req.body.email;
  state.password = req.body.password;

	loginController.login(state,function(){
    req.session.user_email = state.email;
    res.redirect("configuration");
    
	});
  //res.render("login",{message : "Wrong email or password"});
});



module.exports = router;