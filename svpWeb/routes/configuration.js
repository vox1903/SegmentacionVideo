var express = require('express');
var router = express.Router();

var configurationController = require("./../controllers/configurationController.js");

//--STATES--
const state = {
  projects : [],
  videos : [],
  projectName : "",
  videoName : "",
  selection : "allProjects"
} 

function checkLoggedIn(req,res,next){
	if (req.session.user_email){
		next();	
	}else{
		res.redirect('/login');
	}
}

function getProjects(req,res,next){
	configurationController.getProjects(state,function(){
		next();
	});
}

function getVideos(req,res,next){
	configurationController.getVideos(state,function(){
		next();
	});
}


router.get('/', checkLoggedIn, getProjects, function(req, res){
	res.render('configuration',state);
});

router.get('/allProjects', checkLoggedIn, getProjects, function(req, res){
	state.selection = "allProjects";
	res.render('configuration',state);
});

router.get('/allVideos', checkLoggedIn, getVideos, function(req, res){
	state.selection = "allVideos";
	res.render('configuration',state);
});


router.post('/deleteVideo', checkLoggedIn, function(req, res){
	state.selectedVideo = req.body.selectedVideo;
	configurationController.deleteVideo(state, function(){
		res.render("configuration",{message : "Video deleted"});
	});
});

router.post('/deleteProject', checkLoggedIn, function(req, res){
	state.selectedProject = req.body.selectedProject;
	configurationController.deleteProject(state, function(){
		res.render("configuration",{message : "Project deleted"})
	});
});



module.exports = router;






