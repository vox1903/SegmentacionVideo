var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var realCuts = [90, 100, 255, 500, 793, 1100];

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});

var resultController = require("./../controllers/resultController.js");
var groundTruthController = require("./../controllers/groundTruthController.js");

//--STATES--
const state = {
  projectName : "",
  algorithm : "",
  projectState : "",
  averageEnergy : 0,
  executionTime : 0,
  sigma1 : 0,
  sigma2 : 0,
  delta : 0,
  segments : [],
  selectedSegment : "",
  metrics : []
}

function getProjectResult(req, res, next){
  state.projectName = req.params.projectName;
  state.algorithm = req.params.algorithm;
  resultController.getProjectResult(state,function(){
    next();
  });
}

function getSegments(req, res, next){
  state.projectName = req.params.projectName;
  state.algorithm = req.params.algorithm;
  state.segments = [];
  resultController.getSegments(state,function(){
    next();
  });
}


router.get('/project/:projectName/algorithm/:algorithm/', getProjectResult, getSegments, function(req, res){
    if (state.segments.length > 0)
      state.segments[0].selection = true;
    res.render("result" ,state);
});




router.get('/project/:projectName/algorithm/:algorithm/segment=:segmentName', getProjectResult, getSegments, 
  function(req, res){
    for (var i = 0; i < state.segments.length; i ++){
      if (state.segments[i].name == req.params.segmentName)
        state.segments[i].selection = true; 
      else
        state.segments[i].selection = false;
    }
    res.render("result",state); 
});

router.get('/project/:projectName/algorithm/:algorithm/diagram', function(req, res){
  var query = {diagram : ''}
  con.query("call getDiagram('"+req.params.projectName+"','"+req.params.algorithm+"')",
      function (err, result, fields) {
      if (err) throw err;
      if (result[0].length > 0){

      	for (var i in result[0]){
      		query.diagram = result[0][i].path_diagram;
      	}
        res.render('diagram', query);
      }
  });
  
});

router.get('/project/:projectName/algorithm/:algorithm/metrics', function(req, res){
  state.projectName = req.params.projectName;
  state.algorithm = req.params.algorithm;
  groundTruthController.updateMetrics(state, function(){
    res.render('metrics',state);
  });
});

module.exports = router;




