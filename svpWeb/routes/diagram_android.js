var express = require('express');
var router = express.Router();
var url = require('url');

/* GET users listing. */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host: "localhost",
        user: "root",
        password: "password",
        database: "segVideo"
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query("call getDiagram('"+ queryData.name +"','"+ queryData.algorithm +"')",
        function(err,rows,fields){
        if (!err){
            if (rows.length > 0){
                if(rows[0][0].android_image == null){
                    var error = {msg:'null'}
                    res.send(error);
                }
                    var image = rows[0][0];
                    res.send(image);
            } else {
                var error = {msg:'No return'}
                res.send(error);
            }
        } else {
            var error = {msg:'Error, can not execute query'}
            res.send(error);
        }
    });
});

module.exports = router;