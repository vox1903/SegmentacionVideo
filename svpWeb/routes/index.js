var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');


var indexController = require("./../controllers/indexController.js");
var algorithmController = require("./../controllers/algorithmController.js");

//--STATES--
const state = {
  projectName : "",
  originalName : "",
  sigma1Dbf : 0.0,
  sigma2Dbf : 0.0,
  sigma1Dnlm : 0.0,
  sigma2Dnlm : 0.0,
  deltaDbf : 0.0,
  deltaDnlm : 0.0,
  videos : [],
  type : ""
} 

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function (req, file, cb) {
    state.originalName = file.originalname;
    cb(null, file.originalname)
  }
});

var upload = multer({storage : storage}).single('videoUpload');


router.post('/', function (req, res) {

  upload(req, res, function (err) {
    if (err){
      res.render("message",{message : "Can't red file"});
      return;
    }
    state.projectName   = req.body.projectName;
    state.sigma1Dbf     = parseFloat(req.body.sigma1Dbf);
    state.sigma2Dbf     = parseFloat(req.body.sigma2Dbf);
    state.sigma1Dnlm    = parseFloat(req.body.sigma1Dnlm);
    state.sigma2Dnlm    = parseFloat(req.body.sigma2Dnlm);
    state.deltaDbf      = parseFloat(req.body.deltaDbf);
    state.deltaDnlm     = parseFloat(req.body.deltaDnlm);

    if (fs.existsSync("public/projects/" + state.projectName)){
      res.render("message",{message : "Project already exists"});
      return;
    }
      
    //CREATE PROJECT
    fs.mkdirSync("public/projects/" + req.body.projectName);
    
    indexController.createProject(state,function(){

      //ASYNC GENERATION OF ALGORITHMS
      algorithmController.generateSegmentation(state, function(){
      	algorithmController.generateSegmentationDBF(state, function(){
      		algorithmController.generateSegmentationDNLM(state, function(){

      		});
      	});
      });
      res.render("message",{message : "Project created"});
      
    });
  });
});

router.post('/createProject', function (req, res) {

    state.projectName   = req.body.projectName;
    state.originalname  = req.body.fragments;
    state.sigma1Dbf     = parseFloat(req.body.sigma1Dbf);
    state.sigma2Dbf     = parseFloat(req.body.sigma2Dbf);
    state.sigma1Dnlm    = parseFloat(req.body.sigma1Dnlm);
    state.sigma2Dnlm    = parseFloat(req.body.sigma2Dnlm);
    state.deltaDbf      = parseFloat(req.body.deltaDbf);
    state.deltaDnlm     = parseFloat(req.body.deltaDnlm);

    if (fs.existsSync("public/projects/" + state.projectName)){
      res.render("message",{message : "Project already exists"});
      return;
    }
      
    //CREATE PROJECT
    fs.mkdirSync("public/projects/" + req.body.projectName);
    
    indexController.createProject(state,function(){

      //ASYNC GENERATION OF ALGORITHMS
      algorithmController.generateSegmentation(state, function(){
      	algorithmController.generateSegmentationDBF(state, function(){
      		algorithmController.generateSegmentationDNLM(state, function(){

      		});
      	});
      });
      res.render("message",{message : "Project created"});

      res.render("message",{message : "Project created"});
  });
});


router.get('/', function(req, res){
  state.type = "newVideo";
  res.render('index',state);      
});

router.get('/createProject', function(req, res){
  indexController.getVideos(state,function(){
    state.type = "existingVideo";
    res.render('index',state);
  });
});

module.exports = router;
