var express = require('express');
var router = express.Router();

var projectsController = require("./../controllers/projectsController.js");

//--STATES--
const state = {
  projects : [],
  filter : ""
} 

router.get('/', function(req, res){
  	projectsController.getProjects(state,function(){
  		res.render('projects',state);
  	});
});

router.post('/',function(req, res){
	state.filter = req.body.filter;
  	projectsController.searchProjects(state,function(){
  		res.render('projects',state);
  	});
});



module.exports = router;