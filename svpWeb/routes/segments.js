var express = require('express');
var router = express.Router();
var url = require('url');

/* GET users listing. */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host: "localhost",
        user: "root",
        password: "root",
        database: "segVideo"
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query("SELECT segment.name,segment.frame_cut,segment.duration FROM project " +
        "INNER JOIN algorithm_project  ON project.project_id = algorithm_project.project_id AND project.name = '" + queryData.nameProject + "' " +
        "INNER JOIN algorithm  ON algorithm.name = '" + queryData.algorithm + "' AND algorithm_project.algorithm_id = algorithm.algorithm_id " +
        "INNER JOIN segment  ON segment.algorithm_project_id = algorithm_project.algorithm_project_id;",
        function(err,rows,fields){
            if (!err){
                if (rows.length > 0){
                    var row = {segments:rows}
                    res.send(row);
                } else {
                    var error = {msg:'No Segment'}
                    res.send(error);
                }
            } else {
                var error = {msg:'Error, can not execute query'}
                res.send(error);
            }
        });
});

module.exports = router;
