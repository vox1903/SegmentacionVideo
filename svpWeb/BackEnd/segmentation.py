import cv2
import numpy as np
import sys
import math
import matplotlib.pyplot as plt
from time import time
import subprocess
import os.path
import csv

#variable declaration
global listDif
global listCuts
global listNoCuts
global listHistograms
global frameNum
global listScene
global totalFrames
global file_name
global outName
global outPath
global frameCount
global fps
global executionTime
global sigma_r
global sigma_s
global vlambda
global projectName
global segmentsTime
global algorithm
global alg
global route

listDif = []
listCuts = []
listNoCuts = []
listHistograms = []
listScene = []
segmentsTime = []
totalFrames = 0
file_name = ""
outName = ""
outPath = ""
projectName = ""
route = ""
frameCount = 0
fps = 0
executionTime = 0
sigma_r = 0
sigma_s = 0
vlambda = 0

#Normalize the image and calculate the histogram
def normalizeFrame(frame):
    #convert frame from rgb to hsv
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)
        
    #split hsv into channels
    layerH, s, v = cv2.split(frame_hsv)
    
    #get heigth and weight from de image
    height, width = frame_hsv.shape[:2]      
    #normalize the h layer to 0..255
    #cv2.normalize(layerH, layerH, 0, 255, cv2.NORM_MINMAX)
    

    #create array to calc and draw the histogram
    h = np.zeros((300, 256, 3))
    bins = np.arange(256).reshape(256, 1)
    color = [(255, 255, 255)]
    # draw histogram
    h = cv2.calcHist([frame_hsv], [0], None, [256], [0, 255])
    cv2.normalize(h, h, 0, 255, cv2.NORM_MINMAX)
    return h

#Calculate the average of the distances
def calcAverage(array):
    if(len(array)==0):
        return 0
    
    result = 0
    for i in range(len(array)):
        result += array[i]
    return (result / len(array))

#Calculate the standard deviation of the distances
def calcDesv(array):
    if(len(array)==0):
        return 0
    result = 0
    Xi = 0
    average = calcAverage(array)
    for i in range(len(array)):
        Xi = array[i]
        result += (Xi - average) * (Xi - average)
    return math.sqrt(result / len(array))

#check is the value of the distance isa cut
def isCut(average, desv, val):
    if (val >= 0.4):
        return True
    else:
        return False

#get the array of distances between histograms
def getDistances(array):
    i = 0
    while(i+1 < len(array)):
        dist = cv2.compareHist(array[i], array[i+1], cv2.HISTCMP_BHATTACHARYYA)
        listDif.append(dist)
        i += 1
    

#Adds the cuts or no cuts to the list
def getCuts(average, desv, array):
    for i in range(len(array)):
        if(isCut(average, desv, array[i])):
            listCuts.append(array[i])
            listScene.append(i)
        else:
            listNoCuts.append(array[i])

#auxiliry function to delete the scenes with just 1 frame
def deleteScenes(array):
    arrayTemp = array
    i = 0
    while(i+1 < len(arrayTemp)):
        if arrayTemp[i]+1 == arrayTemp[i+1]:
            del array[i+1]
        i += 1

#get the graph of distances beetwen histograms
def getDistancesImage(mode):
    global outName
    global outPath
    global androidDir
    
    if (mode=="0"):
        alg = "original"
    elif (mode=="1"):
        alg = "dbf"
    elif (mode=="2"):
        alg = "dnlm"
    
    name = outPath + outName + "Distances.png"
    #androidDir = "../SegVideo/app/src/main/res/drawable/" + projectName.lower() + alg + ".png"
    plt.plot(listDif)
    plt.ylabel('Distance')
    plt.savefig(name)
    #plt.savefig(androidDir)

def writeCSV():
    global listScene
    global route
    fieldnames = ['frameCut']
    name = route + "/frameCuts.csv"
    with open(name, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for i in range(len(listScene)):
            writer.writerow({'frameCut': str(listScene[i])})
        
#write the video segments
def writeVideo():
    global segmentsTime
    global totalFrames
    global route
    video_format = ".mp4"
    image_format = ".png"
    count = 0
    cap = cv2.VideoCapture(file_name)
    scene = 0
    fileName =  outPath + outName + str(scene) + video_format
    frameCount = cap.get(7)
    totalFrames = frameCount
    listScene.append(int(totalFrames))
    cantFrames = 0
    file = open("info.txt", "w")
    file.write(str(len(listScene)))
    file.write("\n")
    
    
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")

    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))
    out = cv2.VideoWriter(fileName,0x00000021, 28, (frame_width,frame_height))
    while(cap.isOpened()):
        ret, frame = cap.read()
        
        if ret == True:
            if (scene <= len(listScene)):
                
                if (count <= listScene[scene]):
                    # Write the frame into the file 'output.
                    out.write(frame)
                    cantFrames += 1
                    if (cantFrames == 1):
                        #save segment image
                        name = outPath + outName + str(scene) + image_format
                        cv2.imwrite(name, frame)
                        #save project image
                        if (count == 0):
                            name = route + "/projectImage.png"
                            cv2.imwrite(name, frame)
                else:
                    #segName
                    file.write(outName + str(scene) + video_format)
                    file.write(" ")
                    #path
                    file.write(algorithm + outName + str(scene) + video_format)
                    file.write(" ")
                    #frameCut
                    file.write(str(listScene[scene]))
                    file.write(" ")
                    #duration
                    file.write(str((cantFrames//28) + 1 ))
                    file.write(" ")
                    file.write(algorithm + outName + str(scene) + image_format)
                    file.write("\n")
                    segmentsTime.append((cantFrames//28) + 1)
                    scene += 1
                    fileName = outPath + outName + str(scene) + video_format
                    out = cv2.VideoWriter(fileName,0x00000021, 28, (frame_width,frame_height))
                    cantFrames = 0
     
            count += 1
     
        # Break the loop
        else:
            break
        
    cap.release()
    out.release()
    #segName
    file.write(outName + str(scene) + video_format)
    file.write(" ")
    #path
    file.write(algorithm + outName + str(scene) + video_format)
    file.write(" ")
    #frameCut
    file.write(str(listScene[scene]))
    file.write(" ")
    #duration
    file.write(str((cantFrames//28) + 1 ))
    file.write(" ")
    file.write(algorithm + outName + str(scene) + image_format)
    file.write("\n")
    segmentsTime.append((cantFrames//28) + 1)

    aveSeg = calcAverage(segmentsTime)
    desvSeg = calcDesv(segmentsTime)
    file.write(str(round(aveSeg,2)) + "\n")
    file.write(str(round(desvSeg,2)) + "\n")
    
    file.close()
 
    # Closes all the frames
    cv2.destroyAllWindows() 

def process(algName):
    global file_name
    global outName
    global outPath
    global listHistograms

    frameNum = 0

    # Create a VideoCapture object and read from input file
    cap = cv2.VideoCapture(file_name)
    
     
    # Check if camera opened successfully
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")

    # Read until video is completed
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame1 = cap.read()
        if ret == True:
            if(frameNum < 100):
                name = "out" + str(frameNum) + ".png"
          	cv2.imwrite(name, frame1)
                
          	

          	if(algName == "DBF"):
                    
                    subprocess.call(["BackEnd/DBF/src/mycc", "-i", name, sigma_r, sigma_s, vlambda])
                    newName = "out" + str(frameNum) + "_DeWAFF.jpg"
                elif(algName == "DNLM"):
                    if(frameNum < 10):
                        subprocess.call(["BackEnd/DNLM/src/mycc",name, sigma_r, sigma_s, vlambda])
                        newName = "out" + str(frameNum) + "_DeNLM.jpg"
                    
		img = cv2.imread(newName, 1)
                frame1Norm = normalizeFrame(img)
                listHistograms.append(frame1Norm)
                subprocess.call(["rm", name])
                subprocess.call(["rm", newName])
	    else:
                break
           
                
            # Press Q on keyboard to  exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
     
        #Break the loop
        else: 
            break
        frameNum += 1  
    # When everything done, release the video capture object
    cap.release()
     
    # Closes all the frames
    cv2.destroyAllWindows()

    
def videoSegmentation(file_name):
    global totalFrames
    
    frameNum = 0

    # Create a VideoCapture object and read from input file
    cap = cv2.VideoCapture(file_name)

    #get frame count, 7 because is the porperty identifier of the framecount
    #total of frames in the video
    frameCount = cap.get(7)
    totalFrames = frameCount
    fps = cap.get(5)
     
    # Check if camera opened successfully
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")
     
    # Read until video is completed
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame1 = cap.read()
        if ret == True:
            #name = "out" + str(frameNum) + ".png"
            #cv2.imwrite(name, frame1)
            frame1Norm = normalizeFrame(frame1)
                
            listHistograms.append(frame1Norm)
     
        #Break the loop
        else: 
            break
        frameNum += 1
        
    # When everything done, release the video capture object
    cap.release()
     
    # Closes all the frames
    cv2.destroyAllWindows()



def init():
    global projectName
    global file_name
    global outName
    global outPath
    global sigma_r
    global sigma_s
    global vlambda

    global listDif
    global listHistograms
    global listScene
    global algorithm
    global alg
    global route

    algorithms = ["DBF", "DNLM"]
    
    #Check if the filename was provided
    try:
        mode = sys.argv[1]
        projectName = sys.argv[2]
        file_name = sys.argv[3]
        outName = sys.argv[4]
        sigma_r = sys.argv[5]
        sigma_s = sys.argv[6]
        vlambda = sys.argv[7]
    except IndexError:
        print "Error: Missing parameters"
        sys.exit()

    
    route = "public/projects/" + projectName
    file_name = "public/uploads/" + file_name   

    startTime = time()
    #call the correct algorithm depending on mode
    if(mode == "0"):
	subprocess.call(["mkdir", route + "/Original"])
        outPath = route + "/Original/"
        algorithm = "/projects/" + projectName + "/Original/"
        videoSegmentation(file_name)
        alg = "original"
    elif(mode == "1"):
	subprocess.call(["mkdir", route + "/DBF"])
        outPath = route + "/DBF/"
        algorithm = "/projects/" + projectName + "/DBF/"
        process(algorithms[0])
        alg = "dbf"
    elif(mode == "2"):
	subprocess.call(["mkdir", route + "/DNLM"])
        outPath = route + "/DNLM/"
        algorithm = "/projects/" + projectName + "/DNLM/"
        process(algorithms[1])
        alg = "dnlm"
    else:
        print "Error: Ivalid mode"
        sys.exit()

    getDistances(listHistograms)
    ave = calcAverage(listDif)
    desv = calcDesv(listDif)
    getCuts(ave, desv, listDif)
    getDistancesImage(mode)
    deleteScenes(listScene)
    writeVideo()
    writeCSV()

    endTime = time()
    executionTime = endTime - startTime

    aveCuts = calcAverage(listCuts)
    desvCuts = calcDesv(listCuts)
    aveNoCuts = calcAverage(listNoCuts)
    desvNoCuts = calcDesv(listNoCuts)

    file = open("info.txt", "a")
    file.write(str(round(aveCuts,2)) + "\n")
    file.write(str(round(desvCuts,2)) + "\n")
    file.write(str(round(aveNoCuts,2)) + "\n")
    file.write(str(round(desvNoCuts,2)) + "\n")
    file.write(str(round(executionTime, 2)) + "\n")
    file.write(algorithm + outName + "Distances.png\n")
    file.write(projectName.lower() + alg)
    file.close()


##############################################################
init()





