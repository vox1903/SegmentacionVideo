var express = require('express');
var app = express();
var mysql = require('mysql');
var PythonShell = require('python-shell');
var fs = require('fs')

var config = {
	  host     : 'localhost',
	  user     : 'root',
	  password : 'password',
	  database : 'segVideo'
}


var mode = "1";
var ProyectName = "Test5";
var outputName = "seg";
var sigmaR = "14";
var sigmaS = "8";
var lambda = "2.5";
var FileName = "test2.mp4"


var pythonOptions = {
    mode: 'text',
    args: [mode, ProyectName, FileName, outputName, sigmaR, sigmaS, lambda]
};

function saveSegments(pString){
	var string = pString.split(" ");
	var name = string[0];
	var path = string [1];
	var frameCut = string[2];
	var duration = string [3];

	console.log(name, path, frameCut, duration);
	
}

app.get('/read', function(req, res){
	var file = 'info.txt'

	fs.readFile(file, 'utf8', function (err, data) {

	    if (err) {
	    	return console.log(err);
	    }
	 	var dataSplit = data.split("\n");
	 	var Segments = parseInt(dataSplit[0]);

	 	//save each segment
	 	for(var i = 1; i < Segments + 1; i++){
	 		saveSegments(dataSplit[i]);
	 	}

	 	//
	 	var averageScenes = dataSplit[Segments+1];
	 	var desviationScenes = dataSplit[Segments+2];
	 	var averageCuts = dataSplit[Segments+3];
	 	var desviationCuts = dataSplit[Segments+4];
	 	var averageNoCuts = dataSplit[Segments+5];
	 	var desviationNoCuts = dataSplit[Segments+6];
	 	var executionTime = dataSplit[Segments+7];



	 	console.log(executionTime);

	 	res.send("Succesfull")


	});
});


app.get('/test', function (req, res) {
	
	var conn = mysql.createConnection(config);
	conn.connect();
	
	conn.query('select name, path, frameCut, duration from segment', function(err, rows, fields) {
	if (err) throw err;

	res.send(rows);
});
	
	conn.end();

});


//Execute pyton script
app.get('/execute', function(request, response){

	PythonShell.run("segmentation.py", pythonOptions, function (err, results) {
	    if (err) throw err;
	});
	
	console.log(response);
	//response.send("Succesfull");


});

app.listen(5000, function () {
  console.log('Example app listening on port 5000!');
});





