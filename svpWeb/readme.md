# SVP Tool

Video segmentation tool

### Installing

Install with

```
npm install
```

Run with

```
npm start svpWeb
```


## Built With

* [npm](https://www.npmjs.com/) - Packet manager used
* [Express.js](http://expressjs.com/) - Main web framework used


## Authors

* **Juan Jose Guerrero** - 
* **Jostin Marin Mena** - 
* **Lucia Zamora** - 

## License

This project is licensed under the ITCR License
