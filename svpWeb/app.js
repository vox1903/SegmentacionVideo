var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var trailingSlash = require('trailing-slash');
var session = require('express-session');

var index = require('./routes/index');
var projects = require('./routes/projects');
var result = require('./routes/result');
var login = require('./routes/login');
var users = require('./routes/users');
var project = require('./routes/project');
var segments = require('./routes/segments');
var groundTruth = require('./routes/groundTruth');
var configuration = require('./routes/configuration');

var app = express();

//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.enable('strict routing');
app.use(trailingSlash({slash: false}))
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public'), {
  redirect: false
}));


app.use(session({
	secret : "user",
    resave: true,
    saveUninitialized: true
}));


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/index', index);
app.use('/projects', projects);
app.use('/groundTruth', groundTruth);
app.use('/result', result);
app.use('/login', login);
app.use('/configuration', configuration);

//android

//app.use('/users', users);
//app.use('/project', project);
//app.use('/segments', segments);
//app.use('/diagram', diagram);

app.listen(3000);

module.exports = app;
