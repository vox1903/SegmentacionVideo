var fs = require('fs');
var mysql = require('mysql');
var PythonShell = require('python-shell');

const file = 'info.txt';
const outputName = "seg";

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});


//SAVES AN INDIVIDUAL SEGMENT
function saveSegment(pString,pName,pAlgorithm){
  var string = pString.split(" ");
  var name = string[0];
  var path = string [1];
  var frameCut = string[2];
  var duration = string [3];
  var image = string[4];

  con.query("call insertNewSegment('" + pName + "','" + pAlgorithm + "','" + name + "','" + path + "',"
      + parseInt(frameCut) + "," + parseInt(duration) + ",'" + image + "')",
    function(err, result, fields){
    	if (err) throw err;
  	});  
}

//UPDATES THE PROJECT IMAGE
function updateProjectImage(pName, pImage){
  con.query("call updateProjectImage('" + pName + "','" + pImage +  "')",
    function(err, result, fields){
    	if (err) throw err;
  	});
}


module.exports = {
	generateSegmentation : function(state,next) {
		var mode = 0;
		var pythonOptions = {
		    mode: 'text',
		    args: [mode, state.projectName, state.originalName, outputName, "0", "0", "0"],
		    scriptPath:__dirname+"/../BackEnd"

		};
		PythonShell.run("segmentation.py", pythonOptions, function (err, results) {
			if (err) throw err;
			fs.readFile(file, 'utf8', function (err, data){
				if (err) throw err;
				var dataSplit = data.split("\n");
	            var segment = parseInt(dataSplit[0]);
	            
	            for(var i = 1; i < segment + 1; i++){
	              saveSegment(dataSplit[i],state.projectName,'original');
	            }

	            var averageScenes     = parseFloat(dataSplit[segment+1]);
	            var deviationScenes   = parseFloat(dataSplit[segment+2]);
	            var averageCuts       = parseFloat(dataSplit[segment+3]);
	            var deviationCuts     = parseFloat(dataSplit[segment+4]);
	            var averageNoCuts     = parseFloat(dataSplit[segment+5]);
	            var deviationNoCuts   = parseFloat(dataSplit[segment+6]);
	            var executionTime     = parseFloat(dataSplit[segment+7]);
	            var pathDiagram       = dataSplit[segment+8];

	            var projectImage = "/projects/" + state.projectName + "/projectImage.png";
  				updateProjectImage(state.projectName, projectImage);

  				con.query("call setDataProject('" + state.projectName + "'," + "'original',"
	              + averageScenes + ","+ deviationScenes + ","+ averageCuts + ","
	              + deviationCuts + "," + averageNoCuts + "," + deviationNoCuts + ","
	              + executionTime + ",'"+ pathDiagram + "')", function(err, result, fields){

	                if (err) throw err;
	                next();
	            });
			});
		});

	},
	generateSegmentationDBF : function(state,next){
		var mode = "1";
		var pythonOptions = {
		    mode: 'text',
		    args: [mode, state.projectName, state.originalName, outputName, state.sigma1Dbf, 
		    	state.sigma2Dbf, state.deltaDbf],
		    scriptPath:__dirname+"/../BackEnd"
		};
		PythonShell.run("segmentation.py", pythonOptions, function (err, results) {
			fs.readFile(file, 'utf8', function (err, data){
				if (err) throw err;
				var dataSplit = data.split("\n");
	            var segment = parseInt(dataSplit[0]);
	            
	            for(var i = 1; i < segment + 1; i++){
	              saveSegment(dataSplit[i],state.projectName,'dbf');
	            }

	            var averageScenes     = parseFloat(dataSplit[segment+1]);
	            var deviationScenes   = parseFloat(dataSplit[segment+2]);
	            var averageCuts       = parseFloat(dataSplit[segment+3]);
	            var deviationCuts     = parseFloat(dataSplit[segment+4]);
	            var averageNoCuts     = parseFloat(dataSplit[segment+5]);
	            var deviationNoCuts   = parseFloat(dataSplit[segment+6]);
	            var executionTime     = parseFloat(dataSplit[segment+7]);
	            var pathDiagram       = dataSplit[segment+8];

	            con.query("call setDataProject('" + state.projectName + "'," + "'dbf',"
	              + averageScenes + ","+ deviationScenes + ","+ averageCuts + ","
	              + deviationCuts + "," + averageNoCuts + "," + deviationNoCuts + ","
	              + executionTime + ",'"+ pathDiagram + "')", function(err, result, fields){

	                if (err) throw err;
	                next();

	            });

			});
		});
	},
	generateSegmentationDNLM : function(state,next){
		var mode = "2";
		var pythonOptions = {
		    mode: 'text',
		    args: [mode, state.projectName, state.originalName, outputName, state.sigma1Dnlm, 
		    	state.sigma2Dnlm, state.deltaDnlm],
		    scriptPath:__dirname+"/../BackEnd"
		};
		PythonShell.run("segmentation.py", pythonOptions, function (err, results) {
			fs.readFile(file, 'utf8', function (err, data){
				if (err) throw err;
				var dataSplit = data.split("\n");
	            var segment = parseInt(dataSplit[0]);
	            
	            for(var i = 1; i < segment + 1; i++){
	              saveSegment(dataSplit[i],state.projectName,'dnlm');
	            }

	            var averageScenes     = parseFloat(dataSplit[segment+1]);
	            var deviationScenes   = parseFloat(dataSplit[segment+2]);
	            var averageCuts       = parseFloat(dataSplit[segment+3]);
	            var deviationCuts     = parseFloat(dataSplit[segment+4]);
	            var averageNoCuts     = parseFloat(dataSplit[segment+5]);
	            var deviationNoCuts   = parseFloat(dataSplit[segment+6]);
	            var executionTime     = parseFloat(dataSplit[segment+7]);
	            var pathDiagram       = dataSplit[segment+8];

	            con.query("call setDataProject('" + state.projectName + "'," + "'dnlm',"
	              + averageScenes + ","+ deviationScenes + ","+ averageCuts + ","
	              + deviationCuts + "," + averageNoCuts + "," + deviationNoCuts + ","
	              + executionTime + ",'"+ pathDiagram + "')", function(err, result, fields){

	                if (err) throw err;
	                next();

	            });

			});
		});
	}

}

