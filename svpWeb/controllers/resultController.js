var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});


module.exports = {

	getProjectResult : function(state, next){ 
		con.query("call getProjectResult('"+state.projectName+"','"+state.algorithm+"')", 
			function (err, result, fields) {
	      
			if (err) throw err;
			if (result[0].length == 0){next({message : "Project not found"})}

			state.projectState 		= result[0][0].algorithmState;
			state.averageScenes 	= result[0][0].average_scenes;
			state.deviationScenes 	= result[0][0].deviation_scenes;
			state.averageCuts 		= result[0][0].average_cuts;
			state.deviationCuts 	= result[0][0].deviation_cuts;
			state.averageNoCuts 	= result[0][0].average_noCuts;
			state.deviationNoCuts 	= result[0][0].deviation_noCuts;
			state.executionTime 	= result[0][0].execution_time;
			state.sigma1 			= result[0][0].sigma1;
			state.sigma2 			= result[0][0].sigma2;
			state.delta 			= result[0][0].delta;
			next();
	    });
	},

	getSegments : function(state,next){
		con.query("call getSegments('"+state.projectName+"','"+state.algorithm+"')", 
				function (err, result, fields) {
			  
			if (err) throw err;

			for (var i in result[0]){
				state.segments.push(
					{'name' : result[0][i].name ,
				 	'frameCut' : result[0][i].frame_cut,
				 	'duration' : result[0][i].duration,
				 	'path' : result[0][i].path,
					'image': result[0][i].image});
			}
			next();
		});
	}



}