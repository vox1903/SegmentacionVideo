var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});

module.exports = {
	getVideos : function(state,next){
		con.query("call getVideos()",function (err, result, fields) {
			if (err) throw err;
			var query = [];
			for (var i in result[0]){
		      	query.push({'name' : result[0][i].name });
	      	}
	      	state.videos = query;
	      	next();
		});
	},
	getProjects : function(state,next){
		con.query("call getProjects()",function (err, result, fields) {
			if (err) throw err;
			var query = [];
			for (var i in result[0]){
		      	query.push({'name' : result[0][i].name });
	      	}
	      	state.projects = query;
	      	next();
		});
	},
	deleteProject  : function(state,next){
		con.query("call deleteProject('"+ state.selectedProject +"')",function (err, result, fields) {
			if (err) throw err;
	      	next();
		});
	},
	deleteVideo  : function(state,next){
		con.query("call deleteVideo('"+ state.selectedVideo +"')",function (err, result, fields) {
			if (err) throw err;
	      	next();
		});
	}


}