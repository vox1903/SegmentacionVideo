var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});

module.exports = {
	getProjects : function(state,next){
		con.query("call getProjects()",function (err, result, fields) {
			if (err) throw err;
			var query = [];
			for (var i in result[0]){
		      	query.push({'name' : result[0][i].name , 'image' : result[0][i].imageFileName });
	      	}
	      	state.projects = query;
	      	next();
		});
	},
	searchProjects : function(state,next){
		var filter = state.filter;
		con.query("call searchProjects('"+ filter +"')",function (err, result, fields) {
			if (err) throw err;
			var query = [];
			for (var i in result[0]){
		      	query.push({'name' : result[0][i].name , 'image' : result[0][i].imageFileName });
	      	}
	      	state.projects = query;
	      	next();
		});
	}

}