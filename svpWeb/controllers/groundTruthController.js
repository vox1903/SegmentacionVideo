var mysql = require('mysql');
var fs = require('fs');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});

function writeLatex(projectName, result){
    var file = __dirname + '/../public/projects/'+projectName+'/table.tex';
    var stream = fs.createWriteStream(file);
    stream.once('open', function(fd) {
      stream.write("\\documentclass[12pt]{article}\n");
      stream.write("\\begin{document}\n");
      stream.write("\\begin{table}[]\n");
      stream.write("\\centering\n");
      stream.write("\\caption{Metrics}\n");
      stream.write("\\begin{tabular}{|l|l|l|l|l|}\n");
      stream.write("\\hline\n");
      stream.write("\\textbf{-} & \\textbf{recall} & \\textbf{precision} & \\textbf{false positives} & \\textbf{false negatives} ");
      stream.write("\\\\\\hline\n");
      for (var i in result[0]){ 
        var recall = result[0][i].recall;
        var presicion = result[0][i].precision;
        var false_positives = result[0][i].false_positives;
        var false_negatives = result[0][i].false_negatives;

        stream.write("\\textbf{algorithm}");
        stream.write(" & " + recall);
        stream.write(" & " + presicion);
        stream.write(" & " + false_positives);
        stream.write(" & " + false_negatives);
        stream.write(" \\\\\\hline\n");
      }
      stream.write("\\end{tabular}\n");
      stream.write("\\end{table}\n");
      stream.write("\\end{document}\n");
      stream.end();
    });
}


function calcRecall(cuts, stateCuts){
  return 1;
}

function calcPrecision(cuts, stateCuts){
  var truePositive = 0;
  var falsePositive = 0;
  for(var i in cuts){
    var check = stateCuts.indexOf(parseInt(cuts[i]));
    if(check != -1) {
      truePositive++;
    }
    else {
      falsePositive++;
    }
  }
  var res = parseFloat((truePositive) / (truePositive + falsePositive));

  return res;
}

function calcPositives(cuts, stateCuts){
  var falsePositive = 0;
  for(var i in cuts){
    var check = stateCuts.indexOf(cuts[i]);
    if(check == -1) {
      falsePositive++;
    }
  }
  return falsePositive;
}

function calcNegatives(cuts, stateCuts){
  var truePositives = 0;
  for(var i in cuts){
    var check = stateCuts.indexOf(parseInt(cuts[i]));
    if(check != -1) {
      truePositives++;
    }
  }
  var falseNegatives = stateCuts.length - truePositives;
  return falseNegatives;
}


module.exports = {
	updateMetrics : function(state, next){
  		var names = ['Original', 'DBF', 'DNLM'];
  		con.query("call getMetrics('"+state.projectName+"')", function (err, result, fields) {
  			if (err) throw err;
  			if (result[0].length > 0){
  				writeLatex(state.projectName, result);
          state.metrics = [];
  				for (var i in result[0]){
	              state.metrics.push({
	                'name' : names[i],
	                'recall' : result[0][i].recall ,
  	            	'precision' : result[0][i].precision,
  	            	'false_positives' : result[0][i].false_positives,
  	            	'false_negatives' : result[0][i].false_negatives
	              });
	            }
  			}
  			next();
  		});
	},
	generateGroundTruth : function(state, next){
		con.query("call getCuts('"+state.projectName+"','"+state.algorithm+"')", function (err, result, fields) {
			if (err) throw err;
			if (result[0].length == 0){ return; }
	        
	        var cuts = [];
	        for (var i in result[0]){
				    cuts.push(result[0][i].frame_cut);
	        }
	        var recall = calcRecall(cuts, state.cuts);
	        var presicion = calcPrecision(cuts, state.cuts);
	        var false_positives = calcPositives(cuts, state.cuts);
	        var false_negatives = calcNegatives(cuts, state.cuts);
          
          con.query("call updateMetrics('"+state.projectName+"','"+state.algorithm+"',"+recall+","+presicion+","+false_positives+","+false_negatives+")", function (err, result, fields) {
            if (err) throw err;
          });
		});
    

		next();
	}


}