var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "segVideo"
});

module.exports = {
	createProject : function(state, next) {
		con.query("call insertNewProject('"+ state.projectName + "','"
			+ "public/projects/" + state.projectName + "','/images/default.png'," +
			state.sigma1Dbf + "," +
			state.sigma2Dbf + "," +
			state.deltaDbf + "," + 
			state.sigma1Dnlm + "," +
			state.sigma2Dnlm + "," +
			state.deltaDnlm + ")",function (err, result, fields) {

			if (err) throw err;
	      	next();
		});
	},
	getVideos : function(state, next) {
		con.query("call getVideos()", function(err, result, fields){
		    if (err) throw err;	
			for (var i in result[0]){
				state.videos.push({'name' : result[0][i].name });
			}
	      	next();
		});
	}

}

